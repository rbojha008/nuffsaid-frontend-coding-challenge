import React from "react";
import PropTypes from "prop-types";

function Message({ type, data, onClear }) {
  const onClearHandler = () => {
    console.log(data);
    onClear(data.uuid);
  };

  return (
    <div className={`message ${type}`}>
      {data.message}
      <div className="clear" onClick={onClearHandler}>
        Clear
      </div>
    </div>
  );
}

Message.propTypes = {};

export default Message;
