import React from "react";
import PropTypes from "prop-types";

function Empty(props) {
  const {} = props;

  return <div className="empty">There is nothing to display</div>;
}

Empty.propTypes = {};

export default Empty;
