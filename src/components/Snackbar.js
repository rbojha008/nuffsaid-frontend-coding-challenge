import React, { useEffect } from "react";
import PropTypes from "prop-types";

function Snackbar({ open, message, onClose }) {
  if (open === false) {
    return null;
  }

  useEffect(() => {
    setTimeout(() => {
      onClose();
    }, 2000);
  }, [message]);

  return (
    <div className="snackbar">
      <div className="snackbar-content">
        {message}
        <div className="close" onClick={onClose}>
          X
        </div>
      </div>
    </div>
  );
}

Snackbar.propTypes = {};

export default Snackbar;
