import Message from "./Message";

export default function messageHoc(type) {
  return ({ data, onClear }) => <Message type={type} data={data} onClear={onClear} />;
}
