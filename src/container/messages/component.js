import React from "react";
import PropTypes from "prop-types";

import Message from "../../components/Message";
import messageHoc from "../../components/hoc";
import Empty from "../../components/Empty";

let ErrorMsg = messageHoc("error");
let WarningMsg = messageHoc("warning");
let InfoMsg = messageHoc("info");

function Messages({ data, onClear }) {
  const onClearHandler = (msg) => {
    onClear(msg);
  };

  let error = data.filter((d) => d.priority === 1);
  let warning = data.filter((d) => d.priority === 2);
  let info = data.filter((d) => d.priority === 3);

  return (
    <div className="messages">
      <div className="message-col">
        <h3>Error Type 1</h3>
        <span>Count {error.length}</span>
        <div className="message-col-list">
          {error.length === 0 ? (
            <Empty></Empty>
          ) : (
            error.map((d) => {
              return <ErrorMsg key={d.uuid} data={d} onClear={onClearHandler} />;
            })
          )}
        </div>
      </div>

      <div className="message-col">
        <h3>Warning Type 2 </h3>
        <span>Count {warning.length}</span>
        <div className="message-col-list">
          {warning.length === 0 ? (
            <Empty></Empty>
          ) : (
            warning.map((d) => {
              return <WarningMsg key={d.uuid} data={d} onClear={onClearHandler} />;
            })
          )}
        </div>
      </div>

      <div className="message-col">
        <h3>Info Type 3 </h3>
        <span>Count {info.length}</span>
        <div className="message-col-list">
          {info.length === 0 ? (
            <Empty></Empty>
          ) : (
            info.map((d) => {
              return <InfoMsg key={d.uuid} data={d} onClear={onClearHandler} />;
            })
          )}
        </div>
      </div>
    </div>
  );
}

Messages.propTypes = {};

export default Messages;
