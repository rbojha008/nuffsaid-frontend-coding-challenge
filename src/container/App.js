import React, { useEffect, useState } from "react";
import Button from "@material-ui/core/Button";
import PropTypes from "prop-types";

import Api from "../api";

import "./app.css";
import Messages from "./messages";
import Snackbar from "../components/Snackbar";

var apiInstance = (function () {
  let instance = null;

  return {
    getInstance: function (cb) {
      if (instance === null) {
        instance = new Api({
          messageCallback: (messages) => {
            cb(messages);
          },
        });

        return instance;
      }
      return instance;
    },
  };
})();

function App(props) {
  const [messages, setMessages] = useState([]);
  const [start, setStart] = useState(true);
  const [snackMessage, setSnackMessage] = useState("");
  const [snackbarOpen, setSnackbarOpen] = useState(false);

  const api = apiInstance.getInstance((message) => {
    if (message.priority === 1) {
      setSnackbarOpen(true);
      setSnackMessage(message.message);
    }
    setMessages((messages) => [message, ...messages]);
  });

  useEffect(() => {
    console.log("I am called");
    api.start();
  }, []);

  const onStartStopClick = () => {
    if (start) {
      api.stop();
    } else {
      api.start();
    }
    setStart(!start);
  };

  const onClearClick = () => {
    setMessages([]);
  };

  const onMessageClearClick = (msgUuid) => {
    console.log(msgUuid);
    let msg = messages.filter((msg) => msg.uuid !== msgUuid);
    setMessages(msg);
  };

  const handleClose = () => {
    setSnackbarOpen(false);
  };

  return (
    <div className="main-container">
      <h2>nuffsaid.com Coding Challenge</h2>
      <hr />
      <div className="action-buttons">
        <button className="stop" onClick={onStartStopClick}>
          {start ? "Stop" : "Start"}
        </button>
        <button className="clear" onClick={onClearClick}>
          Clear
        </button>
      </div>
      <div className="message-container">
        <Messages data={messages} onClear={onMessageClearClick} />
      </div>
      <Snackbar open={snackbarOpen} onClose={handleClose} message={snackMessage}></Snackbar>
    </div>
  );
}

App.propTypes = {};

export default App;
